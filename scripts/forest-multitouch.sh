#!/bin/bash

set -euo pipefail

date
timestamp_file=$(pwd)/for_tar/timestamp-reference
cd loopback
# PER_LEVEL=100 # for 1 million files, for testing and timing
PER_LEVEL=1000 # for 1 billion files
files=$(seq -w 1 $PER_LEVEL)
for level1 in $(seq -w 1 $PER_LEVEL); do
  mkdir $level1
  cd $level1
  for level2 in $(seq -w 1 $PER_LEVEL); do
    mkdir $level2
    cd $level2
    touch -r $timestamp_file $files
    cd ..
    touch -r $timestamp_file $level2
  done # /level2
  cd ..
  touch -r $timestamp_file $level1
  echo -n . # print a dot, for progress meter
done # /level1
date
