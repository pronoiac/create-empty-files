# using scripts to make many, many files
## why
Maybe you're wary of, or can't compile or run, custom binaries.
Maybe you're wondering "how much slower could it be?"

These were written for Debian on the Pi.


## preparation
I wrote them to do their work in `./loopback`.

* [mkfs-ext4.sh](mkfs-ext4.sh) - make a 1 TB sparse file, format it to ext4

## various attempts
These are ordered, roughly, slowest to fastest.
These times were on an ext4 file system.

| method                               | clock time                    | files/second |
|--------------------------------------|-------------------------------|--------------|
| shell script: `forest-touch.sh`      | 31days (estimated, cancelled) | 375          |
| python script: `create_files.py`     | 1451m43.889s or 24hr11min43s  | 11480        |
| shell script: `forest-tar.sh`        | 1299min16s or 21hr39min16s    | 12830        |
| shell script: `forest-multitouch.sh` | 1157min54s or 19hr17min54sec  | 14390        |

* [`forest-touch.sh`](forest-touch.sh) - run `touch $file` in a loop, 1 billion times
* [`create_files.py`](create_files.py) - creates a file, 1 billion times.
from [Lars Wirzenius, take 1, repo](http://git.liw.fi/billion-files/).
* [`forest-tar.sh`](forest-tar.sh) - build a tar.gz with a million files, then unpack it, a thousand times. 
makes an effort for a consistent timestamp, for better compression.
* [`forest-multitouch.sh`](forest-multitouch.sh) - run `touch 0001 ... 1000` in a loop, 1 million times.
makes an effort for a consistent timestamp, for better compression.
