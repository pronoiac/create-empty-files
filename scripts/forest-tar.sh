#!/bin/bash

set -euo pipefail

cd for_tar
tar_files=$(pwd)

touch timestamp-reference

## level 1: a folder with 1k files
### create files
mkdir level1
cd level1
time for file in $(seq -w 1 1000); do
  touch -r ../timestamp-reference $file
done
### archive
time tar --sort=name -cf - . | gzip -9 > ../l1.tar.gz
cd ..


## level 2: 1k folders, with 1k files each
### create files
mkdir level2 && cd level2
time for folder in $(seq -w 1 1000); do
  mkdir $folder && cd $folder
  tar xzf ../../l1.tar.gz
  cd ..
done
touch -r ../timestamp-reference *
### archive
time tar --sort=name -cf - . | gzip -9 > ../l2.tar.gz.take2
cd ..


## level 3
date
cd loopback
time for folder in $(seq -w 1 1000); do
  mkdir $folder && cd $folder
  tar xzf $tar_files/l2.tar.gz
  cd ..
  touch -r $tar_files/timestamp-reference $folder
  echo -n "." # 1k dots, total
done
