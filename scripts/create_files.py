#!/usr/bin/env python3


import os
import sys
import time


class Progress:
    def __init__(self, goal):
        self.goal = goal
        self.cur = 0
        self.prev = 0

    def inc(self):
        self.cur += 1
        now = time.time()
        if now - self.prev > 1:
            self.prev = now
            self.update()

    def update(self):
        sys.stdout.write(f"\r{self.cur}/{self.goal}")
        sys.stdout.flush()


class Filenames:
    def __init__(self, numfiles, root):
        self.numfiles = numfiles
        self.root = root
        self.perlevel = 1000

    def filenames(self):
        a = b = 0
        for i in range(self.numfiles):
            dirname = f"{self.root}/{a}/{b}"
            yield f"{dirname}", f"{dirname}/file-{i}"
            if (i % self.perlevel) == 0:
                b += 1
                if b > self.perlevel:
                    b = 0
                    a += 1


root = sys.argv[1]
numfiles = int(sys.argv[2])

progress = Progress(numfiles)
filenames = Filenames(numfiles, root)

prevdir = None
for (dirname, filename) in filenames.filenames():
    if dirname != prevdir:
        prevdir = dirname
        os.makedirs(dirname)
    open(filename, "w").close()
    progress.inc()

progress.update()
sys.stdout.write("\n")