#!/bin/bash

set -euo pipefail

date
cd loopback
# PER_LEVEL=100 # for 1 million files, for testing and timing
PER_LEVEL=1000 # for 1 billion files
for level1 in $(seq -w 1 $PER_LEVEL); do
  mkdir $level1
  cd $level1
  for level2 in $(seq -w 1 $PER_LEVEL); do
    mkdir $level2
    cd $level2
     for filenum in $(seq -w 1 $PER_LEVEL); do
       touch $filenum
     done # /filenum
     cd ..
    done # /level2
  cd ..
  echo -n . # print a dot, as progress meter
done # /level1
date
