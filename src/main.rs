use std::{
    fs::{create_dir_all, read, write, File},
    path::{Path, PathBuf},
    process::Command,
    thread::sleep,
    time::Duration,
};

use clap::Parser;
use indicatif::{ProgressBar, ProgressStyle};

fn main() -> anyhow::Result<()> {
    let args = Args::parse();

    if !args.image.exists() {
        create_image(&args.image, &args.fstype, args.image_size)?;
    }
    mount(&args.image, &args.mount_point)?;
    let x = create_files(&args);
    umount(&args.mount_point)?;
    x
}

fn create_image(filename: &Path, fstype: &str, size: u64) -> anyhow::Result<()> {
    let filename = format!("{}", filename.display());

    File::create(&filename)?.set_len(size)?;

    let output = match fstype {
        "ext4" => Command::new("mke2fs")
            .args(["-i", "1024", "-N", "2000000000", "-t", "ext4", "-F", &filename])
            .output(),
        _ => panic!("file system type is not supported: {fstype}"),
    }?;

    if !output.status.success() {
        panic!("failed to create file system image");
    }

    Ok(())
}

fn mount(image: &Path, mount_point: &Path) -> anyhow::Result<()> {
    let output = Command::new("mount").args([image, mount_point]).output()?;
    if !output.status.success() {
        panic!("failed to mount");
    }
    Ok(())
}

fn umount(mount_point: &Path) -> anyhow::Result<()> {
    let output = Command::new("umount").arg(mount_point).output()?;
    if !output.status.success() {
        panic!("failed to umount");
    }
    Ok(())
}

fn create_files(args: &Args) -> anyhow::Result<()> {
    let bar = ProgressBar::new(args.count as u64);
    bar.set_style(
        ProgressStyle::with_template("[{elapsed_precise}] {wide_bar} [{eta_precise}]")?
            .progress_chars("##-"),
    );

    for one in Filenames::new(prev, args.count, args.per_level, &args.mount_point) {
        bar.inc(1);
        if !one.dirname.exists() {
            create_dir_all(&one.dirname)?;
        }
        write(&one.filename, "")?;

        if let Some(ms) = &args.sleep {
            sleep(Duration::from_millis(*ms));
        }
    }

    bar.finish();
    Ok(())
}

fn directory_path(root: &Path, a: usize, b: usize) -> PathBuf {
    root.join(format!("{a}/{b}"))
}

struct One {
    i: usize,
    dirname: PathBuf,
    filename: PathBuf,
}

impl One {
    fn new(i: usize, dirname: PathBuf, filename: PathBuf) -> Self {
        Self {
            i,
            dirname,
            filename,
        }
    }
}

struct Filenames {
    prev: usize,
    goal: usize,
    per_level: usize,
    a: usize,
    b: usize,
    root: PathBuf,
    dirname: PathBuf,
}

impl Filenames {
    fn new(prev: usize, goal: usize, per_level: usize, root: &Path) -> Self {
        Self {
            prev,
            goal,
            per_level,
            a: 0,
            b: 0,
            root: root.into(),
            dirname: directory_path(root, 0, 0),
        }
    }
}

impl Iterator for Filenames {
    type Item = One;
    fn next(&mut self) -> Option<Self::Item> {
        let next = self.prev + 1;
        if next <= self.goal {
            self.prev = next;
            if (next % self.per_level) == 0 {
                self.b += 1;
                if self.b > self.per_level {
                    self.a += 1;
                    self.b = 0;
                }
                self.dirname = directory_path(&self.root, self.a, self.b);
            }
            let filename = self.dirname.join(format!("file-{}.txt", next));
            Some(One::new(next, self.dirname.clone(), filename))
        } else {
            None
        }
    }
}

#[derive(Debug, Parser)]
struct Args {
    #[clap(long)]
    count: usize,

    #[clap(long, default_value = "1000")]
    per_level: usize,

    #[clap(long)]
    sleep: Option<u64>,

    #[clap(long, default_value = "ext4")]
    fstype: String,

    #[clap(long)]
    image: PathBuf,

    #[clap(long, default_value = "1099511627776")]
    image_size: u64,

    #[clap(long, default_value = "/mnt")]
    mount_point: PathBuf,
}
